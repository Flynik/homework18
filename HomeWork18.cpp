﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;
template<typename T>

class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    vector<T> v;
};

int main() 
{
    setlocale(LC_ALL, "rus");
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3);
    a.show();
    cout << "Достаём: " << a.pop() << endl;
    a.show();
    return 0;
}

template<class T> void Stack<T>::push(T element)
{
    v.push_back(element);
}

template<class T> T Stack<T>::pop()
{
    T elem = v.back();
    v.pop_back();
    return elem;
}
template<class T> void Stack<T>::show()
{
    cout << "Stack:";
    for (auto e : v) cout << e << " ";
    cout << endl;
}


